# Sobre
Projeto de estudo da linguagem Java, utilizando JavaFX com JDBC e Banco de dados MySql

## Tecnologias utilizadas e versões

|                     |Versão            |
|---------------------|------------------|
|eclipse-java (x64)   |	2018-12 (4.10.0) |
|SceneBuilder         |	11.0.0           |
|openjfx (x64)        |	11.0.2           |
|jdk (x64)            |	11.0.3           |
|mysql-connector-java |	5.1.47           |

## Script do banco de dados
